#![feature(generic_const_exprs)]
#[macro_use]
extern crate enum_index_derive;

mod os_common_definitions;
mod os_events;
mod os_priority;
fn main() {
    os_events::test();
    let a: os_priority::BasicFifoPrioritizer<3, 6>;
    println!("Hello, world!");
}
