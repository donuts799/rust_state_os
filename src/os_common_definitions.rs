use crate::os_events::Event;

pub type EventIndex = usize;
pub type Priority = usize;

pub enum StateMachineAction {
    None,
}

pub trait StateMachine {
    fn get_prio(&self) -> Priority;
    fn dispatch(&self, evt: Event) -> StateMachineAction;
}

pub trait OSInterface {
    fn subscribe(prio: Priority, event_index: EventIndex);
    fn unsubscribe(prio: Priority, event_index: EventIndex);
}