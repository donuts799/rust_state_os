extern crate enum_index;
use enum_index::{EnumIndex};


#[derive(EnumIndex, Debug)]
pub enum Event {
    Entry,
    Exit,
    MaxEvent,
}

pub fn test() {
    let a = Event::Entry;
    let b = a.enum_index();
    println!("b {0}", b);
}