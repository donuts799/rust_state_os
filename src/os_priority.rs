use std::collections::HashMap;

use crate::os_common_definitions::*;
use crate::os_events::Event;

pub trait Prioritizer {
    fn new(&self, max_prio: Priority, max_event: EventIndex);
    fn subscribe(&self, prio: Priority, event_index: EventIndex);
    fn unsubscribe(&self, prio: Priority, event_index: EventIndex);
    fn getNextAction(&self) -> (Priority, Event);
    fn queueEvent(&self, event: Event);
}

pub struct BasicFifoPrioritizer<const MAX_EVENTS: EventIndex, const MAX_PRIOS: Priority>
where [(); ((MAX_PRIOS - 1) / 8) + 1] : {
    // Size is number of bytes needed for bits rounded up to nearest byte
    subscriptions: [[Event; ((MAX_PRIOS - 1) / 8) + 1]; MAX_EVENTS],
}